-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Ned 28. lis 2021, 00:12
-- Verze serveru: 10.4.21-MariaDB
-- Verze PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `web_sp_ulrych`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `clanek`
--

CREATE TABLE `clanek` (
  `clanek_id` int(11) NOT NULL,
  `autor` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `nazev` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `abstrakt` text COLLATE utf8mb4_czech_ci NOT NULL,
  `datum_vytvoreni` date NOT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `schvalen` tinyint(4) NOT NULL DEFAULT 0,
  `uzivatel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `clanek`
--

INSERT INTO `clanek` (`clanek_id`, `autor`, `nazev`, `abstrakt`, `datum_vytvoreni`, `pdf`, `schvalen`, `uzivatel_id`) VALUES
(12, 'Dobrovodská A.', 'Důležitost větráků v nevětrané místnosti', 'Tento článek se zabývá tématem důležitosti přítomnosti větráků v místnosti, která je nedostatečně (nebo dokonce vůbec není) větraná.', '2021-11-27', 'user18_2021-11-27CET23.47.34_pdf-test.pdf', 1, 18),
(13, 'Dobrovodská A., Bareš M.', 'Inflace a její vliv na ceny větráků', 'Tento článek se zabývá vlivem inflace na ceny větráků, porovnává ceny větráků s doby minulými a predikuje vývoj cen větráků v blízké budoucnosti. Veškeré poznatky v tomto článku jsou podloženy dlouholetými zkušenostmi ve vývoji, nákupu a testování zařízení, které nám ofukují hlavy.', '2021-11-27', 'user18_2021-11-27CET23.50.49_pdf.pdf', 0, 18),
(14, 'Churavý D.', 'Proč jsou větráky dobrý', 'Dnees se podíváme zpátky do minulosti na too, jak se větrácí dostali k nám domůů <3333333333', '2021-11-27', 'user19_2021-11-27CET23.52.26_sample.pdf', 2, 19);

-- --------------------------------------------------------

--
-- Struktura tabulky `recenze`
--

CREATE TABLE `recenze` (
  `recenze_id` int(11) NOT NULL,
  `kvalita_obsahu` double DEFAULT NULL,
  `uroven` double DEFAULT NULL,
  `novost` double DEFAULT NULL,
  `kvalita_jazyka` double DEFAULT NULL,
  `komentar` text COLLATE utf8mb4_czech_ci DEFAULT NULL,
  `datum` date NOT NULL,
  `uzivatel_id` int(11) NOT NULL,
  `clanek_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `recenze`
--

INSERT INTO `recenze` (`recenze_id`, `kvalita_obsahu`, `uroven`, `novost`, `kvalita_jazyka`, `komentar`, `datum`, `uzivatel_id`, `clanek_id`) VALUES
(30, 4, 5, 5, 5, 'Zajímavý a poučný článek. Doporučuji!', '2021-11-27', 21, 12),
(31, 5, 4, 5, 3, 'Super počtení, doporučuji!', '2021-11-27', 22, 12),
(32, 4, 5, 4, 3, 'Nad tímto tématem jsem nikdy nepřemýšlel. Díky za rozšíření obzorů!', '2021-11-27', 23, 12),
(33, 0, 0, 0, 0, 'To je vtip?', '2021-11-27', 23, 14),
(34, 3, 2, 4, 5, 'Místy rozporuplné, pojmy se míchají s dojmy... Chtělo by to dotáhnout!', '2021-11-27', 21, 13),
(35, 4, 3, 1, 3, 'Hmmm... Chtělo by to dotáhnout, ale začátek slibný.', '2021-11-27', 23, 13);

-- --------------------------------------------------------

--
-- Struktura tabulky `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `role`
--

INSERT INTO `role` (`role_id`, `nazev`) VALUES
(1, 'autor'),
(2, 'recenzent'),
(3, 'admin'),
(4, 'superadmin');

-- --------------------------------------------------------

--
-- Struktura tabulky `uzivatel`
--

CREATE TABLE `uzivatel` (
  `uzivatel_id` int(11) NOT NULL,
  `jmeno` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL,
  `prijmeni` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL,
  `login` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL,
  `heslo` text COLLATE utf8mb4_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `adresa` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `zablokovan` tinyint(1) DEFAULT 0,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `uzivatel`
--

INSERT INTO `uzivatel` (`uzivatel_id`, `jmeno`, `prijmeni`, `login`, `heslo`, `email`, `adresa`, `zablokovan`, `role_id`) VALUES
(17, 'superadmin', 'superadmin', 'superadmin', '$2y$10$ckomSkeyTLDqsoab72X/XORd1XHkWSTlv8/1Z/PvHPMSLianVOQ.i', 'superadmin@konference.cz', 'Super Admin\r\nAdminová 19\r\nAdminov\r\n723 65', 0, 4),
(18, 'Alena', 'Dobrovodská', 'alena.dobrovodska', '$2y$10$QmVZDcHh1.WB6/NIAF2.ZOrMaYrYKw8NMHdHQmBJpKDb54ymMWYwq', 'alena.dobrovodska@konference.cz', 'Alena Dobrovodská\r\nPod Záhonem 36\r\nZáhonov\r\n491 24', 0, 1),
(19, 'David', 'Churavý', 'david.churavy', '$2y$10$nCFAKXlRpi9GA0H3DPVZV.cKORH47jlDh5/9kQIL2NJ1O/JhyWxcG', 'david.churavy@konference.cz', 'David Churavý\r\nTřešňová 66\r\nTřešňov\r\n226 86', 1, 1),
(20, 'Matěj', 'Bareš', 'matej.bares', '$2y$10$.VXeLh7kAkrEBLuplJtkPenaWi/Q5YTBY8YoUwsTWNqIi1C.ayr/C', 'matej.bares@konference.cz', 'Matěj Bareš\r\nOhnivá 1\r\nPeklov\r\n666 66', 0, 1),
(21, 'Zdeňka', 'Vacková', 'zdenka.vackova', '$2y$10$R6VYcyvu1h3ECm.vEYScf.F04lwkpfW4DSTI8ORmAIs3R/0BGZufi', 'zdenka.vackova@konference.cz', 'Zdeňka Vacková\r\nŠikmá 13\r\nTvarov\r\n395 00', 0, 2),
(22, 'Ivan', 'Mikoláš', 'ivan.mikolas', '$2y$10$oyfSQhU/WwR1sqSZCNt5QeIn5XAHTEvIwJS15J9ankcZsogwQUAuW', 'ivan.mikolas@konference.cz', 'Ivan Mikoláš\r\nPosvátná 14\r\nMikulov\r\n284 23', 0, 2),
(23, 'Radek', 'Kulich', 'radek.kulich', '$2y$10$IzwJb/mHB1dslcVqrv.z7uiuTlDgTrZXZN.WQG42.H5M5lNBV/RjG', 'radek.kulich@konference.cz', 'Radek Kulich\r\nZimní 19\r\nSněhov\r\n100 20', 0, 2),
(24, 'Jana', 'Smetáková', 'jana.smetakova', '$2y$10$zCXOu8xCPrStTy3RocBE6OsElV.nv8maLq8hZCCIaHK1Gxr4Fywxu', 'jana.smetakova@konference.cz', 'Jana Smetáková\r\nSmetáková 86\r\nSmetákov\r\n198 00', 0, 3),
(25, 'Jiří', 'Veverka', 'jiri.veverka', '$2y$10$8TmXwyspfyP60E38SNXWz.mCpJzryhJsaTuJWiW7Kd6m2u2CT7oGC', 'jiri.veverka@konference.cz', 'Jiří Veverka\r\nVeverková 45\r\nVeverkov\r\n386 23', 0, 3);

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `clanek`
--
ALTER TABLE `clanek`
  ADD PRIMARY KEY (`clanek_id`),
  ADD KEY `uzivatel_id` (`uzivatel_id`);

--
-- Indexy pro tabulku `recenze`
--
ALTER TABLE `recenze`
  ADD PRIMARY KEY (`recenze_id`),
  ADD KEY `uzivatel_id` (`uzivatel_id`),
  ADD KEY `clanek_id` (`clanek_id`);

--
-- Indexy pro tabulku `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexy pro tabulku `uzivatel`
--
ALTER TABLE `uzivatel`
  ADD PRIMARY KEY (`uzivatel_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `clanek`
--
ALTER TABLE `clanek`
  MODIFY `clanek_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pro tabulku `recenze`
--
ALTER TABLE `recenze`
  MODIFY `recenze_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pro tabulku `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pro tabulku `uzivatel`
--
ALTER TABLE `uzivatel`
  MODIFY `uzivatel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `clanek`
--
ALTER TABLE `clanek`
  ADD CONSTRAINT `clanek_ibfk_1` FOREIGN KEY (`uzivatel_id`) REFERENCES `uzivatel` (`uzivatel_id`);

--
-- Omezení pro tabulku `recenze`
--
ALTER TABLE `recenze`
  ADD CONSTRAINT `recenze_ibfk_1` FOREIGN KEY (`uzivatel_id`) REFERENCES `uzivatel` (`uzivatel_id`),
  ADD CONSTRAINT `recenze_ibfk_2` FOREIGN KEY (`clanek_id`) REFERENCES `clanek` (`clanek_id`);

--
-- Omezení pro tabulku `uzivatel`
--
ALTER TABLE `uzivatel`
  ADD CONSTRAINT `uzivatel_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
