<?php

/** Adresa serveru. */
define("DB_SERVER","localhost");
/** Nazev databaze. */
define("DB_NAME","web_sp_ulrych");
/** Uzivatel databaze. */
define("DB_USER","root");
/** Heslo uzivatele databaze */
define("DB_PASS","");

/** Cesta ke korenovemu adresari  */
define('SITE_ROOT', realpath(dirname(__FILE__)));

/* Nazvy tabulek v databazi */
define("TABLE_UZIVATEL", "uzivatel");
define("TABLE_ROLE", "role");
define("TABLE_CLANEK", "clanek");
define("TABLE_RECENZE", "recenze");

/* Uzivatelske role aplikace */
define("ROOT", 0);
define("AUTOR", 1);
define("RECENZENT", 2);
define("ADMIN", 3);
define("SUPERADMIN", 4);

/* Klasifikace typu zabezpeceni stranky */
define("greater", ">");
define("equals", "==");

/** Klic defaultni webove stranky. */
const DEFAULT_WEB_PAGE_KEY = "uvod";

/** Dostupne webove stranky. */
const WEB_PAGES = array(//// Uvodni stranka ////
    "uvod" => array(
        "title" => "Úvod",
        "controller_class_name" => \websp\Controllers\IntroController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_INTRODUCTION,
        "role" => ROOT,
        "type" => greater
    ),
    "clanky" => array(
        "title" => "Články",
        "controller_class_name" => \websp\Controllers\ArticleController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_ARTICLE,
        "role" => ROOT,
        "type" => greater
    ),
    "login" => array(
        "title" => "Přihlášení",
        "controller_class_name" => \websp\Controllers\LoginController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_LOGIN,
        "role" => ROOT,
        "type" => equals
    ),
    "registration" => array(
        "title" => "Registrace",
        "controller_class_name" => \websp\Controllers\RegistrationController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_REGISTRATION,
        "role" => ROOT,
        "type" => equals
    ),
    "muj_profil" => array(
        "title" => "Můj profil",
        "controller_class_name" => \websp\Controllers\MyProfileController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_MY_PROFILE,
        "role" => AUTOR,
        "type" => greater
    ),
    "uzivatele" => array(
        "title" => "Uživatelé",
        "controller_class_name" => \websp\Controllers\UserManagementController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_USER_MANAGEMENT,
        "role" => ADMIN,
        "type" => greater
    ),
    "novy_clanek" => array(
        "title" => "Nový článek",
        "controller_class_name" => \websp\Controllers\NewArticleController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_NEW_ARTICLE,
        "role" => AUTOR,
        "type" => equals
    ),
    "moje_clanky" => array(
        "title" => "Moje články",
        "controller_class_name" => \websp\Controllers\MyArticlesController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_MY_ARTICLES,
        "role" => AUTOR,
        "type" => equals
    ),
    "sprava_clanku" => array(
        "title" => "Správa článků",
        "controller_class_name" => \websp\Controllers\ArticleManagementController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_ARTICLE_MANAGEMENT,
        "role" => ADMIN,
        "type" => greater
    ),
    "moje_recenze" => array(
        "title" => "Moje recenze",
        "controller_class_name" => \websp\Controllers\MyReviewsController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_MY_REVIEWS,
        "role" => RECENZENT,
        "type" => equals
    ),
    "pristup_odepren" => array(
        "title" => "Přístup odepřen",
        "controller_class_name" => \websp\Controllers\AccessDeniedController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_ACCESS_DENIED,
        "role" => ROOT,
        "type" => greater
    ),
    "nedostatecna_prava" => array(
        "title" => "Nedostatečné oprávnění",
        "controller_class_name" => \websp\Controllers\InsufficientRightsController::class,
        "view_class_name" => \websp\Views\BaseTemplate::class,
        "template_type" => \websp\Views\BaseTemplate::PAGE_INSUFFICIENT_RIGHTS,
        "role" => AUTOR,
        "type" => greater
    )
);

?>
