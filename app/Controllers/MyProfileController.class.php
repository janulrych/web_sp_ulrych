<?php

namespace websp\Controllers;

use websp\Models\DatabaseModel;
use websp\Models\UserModel;

/**
 * Ovladac pro vypsani stranky se spravou uzivatelskeho profilu.
 * @package kivweb\Controllers
 */
class MyProfileController implements IController{

    /** @var UserModel $um Model pro praci s uzivatelem. */
    private $um;

    /**
     * Konstruktor tridy MyProfileController.
     * Nainicializuje tridni promenne.
     */
    public function __construct(){
        $this->um = new UserModel();
    }

    /**
     * Vrati obsah stranky se spravou uzivatelskeho profilu.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;
        $tplData['user'] = $this->um->getLoggedUserData();

        return $tplData;
    }
}

?>
