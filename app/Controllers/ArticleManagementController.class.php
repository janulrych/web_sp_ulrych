<?php

namespace websp\Controllers;

use websp\Models\DatabaseModel;

/**
 * Ovladac pro vypsani stranky se spravou clanku.
 * @package kivweb\Controllers
 */
class ArticleManagementController implements IController{

    /** @var DatabaseModel $db  Databazovy model. */
    private $db;

    /**
     * Konstruktor tridy ArticleManagementController.
     * Nainicalizuje tridni promenne.
     */
    public function __construct(){
        $this->db = new DatabaseModel();
    }

    /**
     * Vrati obsah stranky se spravou clanku.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;
        $tplData['data'] = $this->db->getAllArticlesWithReviews();

        return $tplData;
    }
}

?>
