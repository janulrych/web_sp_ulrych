<?php

namespace websp\Controllers;

use websp\Models\DatabaseModel;

/**
 * Ovladac pro vypsani stranky s clanky prihlaseneho uzivatele.
 * @package kivweb\Controllers
 */
class MyArticlesController implements IController{

    /** @var DatabaseModel $db  Databazovy model. */
    private $db;

    /**
     * Konstruktor tridy MyArticlesController.
     * Nainicializuje tridni promenne.
     */
    public function __construct(){
        $this->db = new DatabaseModel();
    }

    /**
     * Vrati obsah stranky s clanky prihlaseneho uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;
        $tplData['articles'] = $this->db->getUserArticles();

        return $tplData;
    }
}

?>
