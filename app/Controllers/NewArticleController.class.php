<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani stranky pro novy clanek.
 * @package kivweb\Controllers
 */
class NewArticleController implements IController{

    /**
     * Vrati obsah uvodni stranky.
     * @param string $pageTitle     Nazev stranky.
     * @return array                Vytvorena data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }
}

?>
