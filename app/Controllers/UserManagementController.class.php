<?php

namespace websp\Controllers;

use websp\Models\DatabaseModel;

/**
 * Ovladac pro vypsani stranky se spravou uzivatelu.
 * @package kivweb\Controllers
 */
class UserManagementController implements IController{

    /** @var DatabaseModel $db  Databazovy model. */
    private $db;

    /**
     * Konstruktor tridy UserManagementController.
     * Nainicalizuje tridni promenne.
     */
    public function __construct() {
        $this->db = new DatabaseModel();
    }

    /**
     * Vrati obsah stranky se spravou uzivatelu.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;
        $tplData['users'] = $this->db->getAllUsers();
        $tplData['roles'] = $this->db->getAllRoles();

        return $tplData;
    }
}

?>
