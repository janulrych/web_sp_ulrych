<?php

namespace websp\Controllers;

use websp\Models\DatabaseModel;

/**
 * Ovladac pro vypsani stranky s recenzemi prihlaseneho uzivatele.
 * @package kivweb\Controllers
 */
class MyReviewsController implements IController{

    /** @var DatabaseModel $db  Databazovy model. */
    private $db;

    /**
     * Konstruktor tridy MyReviewsController.
     * Nainicializuje tridni promenne.
     */
    public function __construct(){
        $this->db = new DatabaseModel();
    }

    /**
     * Vrati obsah stranky s recenzemi prihlaseneho uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;
        $tplData['data'] = $this->db->getUserReviews();

        return $tplData;
    }
}

?>
