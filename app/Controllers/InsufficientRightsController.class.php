<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani chybove stranky pro nedostatecna prava uzivatele.
 * @package kivweb\Controllers
 */
class InsufficientRightsController implements IController{

    /**
     * Vrati obsah chybove stranky pro nedostatecna prava uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }
}

?>
