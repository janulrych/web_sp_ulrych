<?php

namespace websp\Controllers;

/**
 * Rozhrani pro vsechny ovladace (kontrolery) aplikace.
 * @package kivweb\Controllers
 */
interface IController{

    /**
     * Zajisti vypsani prislusne stranky.
     *
     * @param string $pageTitle     Titulek stanky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array;

}

?>
