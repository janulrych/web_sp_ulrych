<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani uvodni stranky.
 * @package kivweb\Controllers
 */
class IntroController implements IController{

    /**
     * Vrati obsah uvodni stranky.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }
}

?>
