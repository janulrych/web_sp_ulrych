<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani stranky s registraci uzivatele.
 * @package kivweb\Controllers
 */
class RegistrationController implements IController{

    /**
     * Vrati obsah stranky s registraci uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }
}

?>
