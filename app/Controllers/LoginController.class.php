<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani stranky pro prihlaseni uzivatele.
 * @package kivweb\Controllers
 */
class LoginController implements IController {

    /**
     * Vrati obsah stranky pro prihlaseni uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }

}

?>
