<?php

namespace websp\Controllers;

/**
 * Ovladac pro vypsani chybove stranky pro neprihlaseneho uzivatele.
 * @package kivweb\Controllers
 */
class AccessDeniedController implements IController{

    /**
     * Vrati obsah chybove stranky pro neprihlaseneho uzivatele.
     * @param string $pageTitle     Titulek stranky.
     * @return array                Data pro sablonu.
     */
    public function show(string $pageTitle): array{
        $tplData = [];

        $tplData['title'] = $pageTitle;

        return $tplData;
    }
}

?>
