<?php

namespace websp;

use websp\Controllers\IController;
use websp\Models\DatabaseModel;
use websp\Models\UserModel;
use websp\Views\IView;

/**
 * Vstupni bod webove aplikace.
 */
class AppStart{

    /** @var UserModel $um  Model pro praci s uzivatelem. */
    private $um;

    /**
     * Konstruktor tridy AppStart.
     * Nainicializuje tridni promenne.
     */
    public function __construct(){
        $this->um = new UserModel();
    }

    /**
     * Spusti vebovou aplikaci na zaklade parametru ziskaneho z URL.
     * Zkontroluje prihlaseni uzivatele a jeho pristupova prava na danou stranku.
     */
    public function appStart(){
        if(isset($_GET["page"]) && array_key_exists($_GET["page"], WEB_PAGES)){
            $pageKey = $_GET["page"];
        } else {
            $pageKey = DEFAULT_WEB_PAGE_KEY;
        }

        $pageInfo = WEB_PAGES[$pageKey];

        if($this->um->isUserLogged()){
            if($pageInfo['type'] == greater){
                if(!($this->um->getUserRole() >= $pageInfo['role'])){
                    header("Location: ?page=nedostatecna_prava");
                    die;
                }
            } else if($pageInfo['type'] == equals){
                if($this->um->getUserRole() != $pageInfo['role']){
                    header("Location: ?page=nedostatecna_prava");
                    die;
                }
            }
        } else {
            if($pageInfo['role'] != ROOT){
                header("Location: ?page=pristup_odepren");
                die;
            }
        }

        /** @var IController $controller  Ovladac prislusne stranky. */
        $controller = new $pageInfo["controller_class_name"];
        $tplData = $controller->show($pageInfo["title"]);

        /** @var IView $view  Sablona prislusne stranky. */
        $view = new $pageInfo["view_class_name"];
        $view->printOutput($tplData, $pageInfo["template_type"]);
    }
}

?>
