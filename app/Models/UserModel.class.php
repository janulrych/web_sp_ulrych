<?php

namespace websp\Models;

/**
 * Trida pro praci s databazi.
 */
class UserModel{

    /** @var Sessions $session  Sprava session. */
    private $session;

    /** @var string $userSessionKey  Klic k ID prihlaseneho uzivatele. */
    private $userSessionKey = "logged_in_user_id";

    /** @var DatabaseModel  Databazovy model. */
    private $db;

    /**
     * Konstruktor tridy DatabaseModel.
     * Nainicalizuje tridni promenne.
     */
    public function __construct(){
        $this->session = new Sessions();
        $this->db = new DatabaseModel();
    }

    /**
     * Prida noveho uzivatele do databaze.
     * @param string $jmeno         Jmeno uzivatele.
     * @param string $prijmeni      Prijmeni uzivatele.
     * @param string $login         Login uzivatele.
     * @param string $heslo         Heslo uzivatele v hash podobe.
     * @param string $email         Email uzivatele.
     * @param string $adresa        Adresa uzivatele.
     * @return bool                 True v pripade, ze se pridani uzivatele podarilo.
     */
    public function addNewUser(string $jmeno, string $prijmeni, string $login, string $heslo, string $email, string $adresa){
        $arguments = $this->replaceHTML($jmeno, $prijmeni, $login, $email, $adresa);
        $insertStatement = "jmeno, prijmeni, login, heslo, email, adresa, role_id";
        $insertValues = "'$arguments[0]', '$arguments[1]', '$arguments[2]', '$heslo', '$arguments[3]', '$arguments[4]', 1";
        return $this->db->insertIntoTable(TABLE_UZIVATEL, $insertStatement, $insertValues);
    }

    /**
     * Zjisti, zda uzivatel s danym loginem v databazi existuje.
     * @param string $login     Hledany login.
     * @return bool             True v pripade, ze uzivatel s danym loginem v databazi existuje.
     */
    public function userLoginExists(string $login): bool{
        $arguments = $this->replaceHTML($login);
        $where = "login='$arguments[0]'";
        $user = $this->db->selectFromTable(TABLE_UZIVATEL, $where);
        if(count($user)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Upravi uzivatele v databazi.
     * @param int $uzivatel_id      ID upravovaneho uzivatele.
     * @param string $jmeno         Upravene jmeno uzivatele.
     * @param string $prijmeni      Upravene prijmeni uzivatele.
     * @param string $email         Upraveny email uzivatele.
     * @param string $adresa        Upravena adresa uzivatele.
     * @return bool                 True v pripade, ze se uprava uzivatele podarila.
     */
    public function updateUser(int $uzivatel_id, string $jmeno, string $prijmeni, string $email, string $adresa){
        $arguments = $this->replaceHTML($uzivatel_id, $jmeno, $prijmeni, $email, $adresa);
        $updateStatementWithValues = "jmeno='$arguments[1]', prijmeni='$arguments[2]', email='$arguments[3]', adresa='$arguments[4]'";
        $whereStatement = "uzivatel_id=$arguments[0]";
        return $this->db->updateInTable(TABLE_UZIVATEL, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Upravi heslo uzivatele.
     * @param string $uzivatel_id       ID uzivatele, jehoz heslo se ma upravit.
     * @param string $heslo             Nove heslo.
     * @return bool                     True v pripade, ze se uprava hesla podarila.
     */
    public function updateUserPassword(string $uzivatel_id, string $heslo){
        $arguments = $this->replaceHTML($uzivatel_id);
        $updateStatementWithValues = "heslo='$heslo'";
        $whereStatement = "uzivatel_id=$arguments[0]";
        return $this->db->updateInTable(TABLE_UZIVATEL, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Nastavi uzivateli novou roli.
     * @param string $uzivatel_id       ID uzivatele.
     * @param int $role_id              ID role.
     * @return bool                     True v pripade, ze se nastaveni role podarilo.
     */
    public function updateUserRole(string $uzivatel_id, string $role_id){
        $arguments = $this->replaceHTML($uzivatel_id, $role_id);
        $updateStatementWithValues = "role_id='$arguments[1]'";
        $whereStatement = "uzivatel_id=$arguments[0]";
        return $this->db->updateInTable(TABLE_UZIVATEL, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Zablokuje uzivatele.
     * @param string $uzivatel_id       ID uzivatele.
     * @return bool                     True v pripade, ze se zablokovani uzivatele podarilo.
     */
    public function blockUser(string $uzivatel_id){
        $arguments = $this->replaceHTML($uzivatel_id);
        $updateStatementWithValues = "zablokovan='1'";
        $whereStatement = "uzivatel_id=$arguments[0]";
        return $this->db->updateInTable(TABLE_UZIVATEL, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Odblokuje uzivatele.
     * @param string $uzivatel_id   ID uzivatele.
     * @return bool                 True v pripade, ze se odblokovani uzivatele podarilo.
     */
    public function unblockUser(string $uzivatel_id){
        $arguments = $this->replaceHTML($uzivatel_id);
        $updateStatementWithValues = "zablokovan='0'";
        $whereStatement = "uzivatel_id=$arguments[0]";
        return $this->db->updateInTable(TABLE_UZIVATEL, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Pokusi se prihlasit uzivatele.
     * @param string $login     Login uzivatele.
     * @param string $heslo     Heslo uzivatele.
     * @return bool             True v pripade, ze je uzivatel prihlasen.
     */
    public function userLogin(string $login, string $heslo){
        $where = "login='$login'";
        $user = $this->db->selectFromTable(TABLE_UZIVATEL, $where);

        if(password_verify($heslo, $user[0]['heslo'])){
            $this->session->addSession($this->userSessionKey, $user[0]['uzivatel_id']);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vrati ID role uzivatele.
     * @return mixed    ID role uzivatele.
     */
    public function getUserRole(){
        $uzivatel = $this->getLoggedUserData();
        return $uzivatel['role_id'];
    }

    /**
     * Vrati ID uzivatele.
     * @return mixed|null   ID uzivatele.
     */
    public function getUserID(){
        return $this->session->readSession($this->userSessionKey);
    }

    /**
     * Odhlasi uzivatele.
     */
    public function userLogout(){
        unset($_SESSION[$this->userSessionKey]);
    }

    /**
     * Zjisti, zda je uzivatel prihlasen.
     * @return bool     True v pripade, ze je uzivatel prihlasen.
     */
    public function isUserLogged(){
        return isset($_SESSION[$this->userSessionKey]);
    }

    /**
     * V pripade, ze je uzivatel prihlasen, tak se pokusi ziskat jeho data. Pokud se to nepodari, uzivatele odhlasi a vrati null.
     * V pripade, ze data uzivatele ziska, tak je vrati.
     * @return mixed|null       Data uzivatele nebo null v pripade, ze se je nepodari ziskat.
     */
    public function getLoggedUserData(){
        if($this->isUserLogged()){
            $userId = $_SESSION[$this->userSessionKey];
            if($userId == null) {
                $this->userLogout();
                return null;
            } else {
                $userData = $this->db->selectFromTable(TABLE_UZIVATEL, "uzivatel_id=$userId");
                if(empty($userData)){
                    $this->userLogout();
                    return null;
                } else {
                    return $userData[0];
                }
            }
        } else {
            return null;
        }
    }

    /**
     * Nahradi HTML znaky specialnimi znaky.
     * @param string ...$arguments      Argumenty na kontrolu.
     * @return array                    Pole zkonstrolovanych argumentu.
     */
    public function replaceHTML(string... $arguments){
        $replacedArgs = array();
        foreach ($arguments as $argument){
            array_push($replacedArgs, htmlspecialchars($argument));
        }
        return $replacedArgs;
    }
}
?>
