<?php

namespace websp\Models;

use PDO;

/**
 * Trida pro praci s databazi.
 */
class DatabaseModel{

    /** @var Sessions $session  Sprava session. */
    private $session;

    /** @var \PDO $pdo  Objekt pro praci s databazi. */
    private $pdo;

    /** @var string $userSessionKey  Klic k ID prihlaseneho uzivatele. */
    private $userSessionKey = "logged_in_user_id";


    /**
     * Konstruktor tridy DatabaseModel.
     * Nainicalizuje tridni promenne.
     */
    public function __construct(){
        $this->session = new Sessions();
        $this->pdo = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASS);
        $this->pdo->exec("set names utf8");
    }

    /**
     * Provede dotaz a vrati ziskana data.
     * V pripade chybi ji vypise a vraci null.
     * @param string $query         Dotaz v jazyce SQL.
     * @return PDOStatement|null    Vysledek dotazu.
     */
    private function executeQuery(string $query): \PDOStatement|null{
        $res = $this->pdo->query($query);
        if ($res) {
            return $res;
        } else {
            $error = $this->pdo->errorInfo();
            echo $error[2];
            return null;
        }
    }

    private function executeQuerySafe(string $sql, array $params): array|null{
        $dotaz = $this->pdo->prepare($sql);
        $res = $dotaz->execute($params);
        if ($res) {
            return $dotaz->fetchAll();
        } else {
            $error = $this->pdo->errorInfo();
            echo $error[2];
            return null;
        }
    }

    public function selectFromTableSafe(string $sql, array $params): array{
        $obj = $this->executeQuerySafe($sql, $params);

        if($obj == null){
            return [];
        }

        return $obj;
    }

    /**
     * V dane tabulce vykona prikaz SELECT.
     * @param string $tableName         Nazev tabulky, ze ktere se ma cist.
     * @param string $whereStatement    Jake zaznamy se v tabulce maji hledat. Vychozi "".
     * @param string $orderByStatement  Jak se maji nalezene zaznamy radit. Vychozi "".
     * @return array                    Pole ziskanych zaznamu.
     */
    public function selectFromTable(string $tableName, string $whereStatement = "", string $orderByStatement = ""): array{
        $query = "SELECT * FROM ".$tableName .(($whereStatement == "") ? "" : " WHERE $whereStatement") .(($orderByStatement == "") ? "" : " ORDER BY $orderByStatement");

        $obj = $this->executeQuery($query);

        if($obj == null){
            return [];
        }

        return $obj->fetchAll();
    }

    /**
     * V dane tabulce vykona prikaz INSERT.
     * @param string $tableName         Nazev tabulky, do ktere se ma pridat zaznam.
     * @param string $insertStatement   Nazvy sloupcu, do kterych se bude vkladat.
     * @param string $insertValues      Hodnoty sloupcu, do kterych se bude vkladat.
     * @return bool                     True v pripade, ze vlozeni probehlo uspesne.
     */
    public function insertIntoTable(string $tableName, string $insertStatement, string $insertValues): bool{
        $query = "INSERT INTO $tableName($insertStatement) VALUES ($insertValues)";
        $obj = $this->executeQuery($query);

        if($obj == null){
            return false;
        } else {
            return true;
        }
    }

    /**
     * V dane tabulce vykona prikaz UPDATE.
     * @param string $tableName                     Nazev tabulky, ve ktere se ma upravit zaznam (zaznamy).
     * @param string $updateStatementWithValues     Jake sloupce se maji upravit na jake hodnoty.
     * @param string $whereStatement                Jako zaznamy se maji upravit.
     * @return bool                                 True v pripade, ze uprava probehla uspesne.
     */
    public function updateInTable(string $tableName, string $updateStatementWithValues, string $whereStatement): bool{
        $query = "UPDATE $tableName SET $updateStatementWithValues WHERE $whereStatement";
        $obj = $this->executeQuery($query);

        if($obj == null){
            return false;
        } else {
            return true;
        }
    }

    /**
     * V dane tabulce vykona prikaz DELETE.
     * @param string $tableName         Nazev tabulky, ze ktere se ma odstranit zaznam (zaznamy).
     * @param string $whereStatement    Jake zaznamy se maji smazat.
     * @return bool                     True v pripade, ze mazani probehlo uspesne.
     */
    public function deleteFromTable(string $tableName, string $whereStatement): bool{
        $query = "DELETE FROM $tableName WHERE $whereStatement";
        $obj = $this->executeQuery($query);

        if($obj == null){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Prida novy clanek do databaze.
     * @param string $nazev         Nazev clanku.
     * @param string $autor         Autor clanku.
     * @param string $pdf           Cesta k pdf souboru clanku.
     * @param string $abstrakt      Abstrakt clanku.
     * @return bool                 True v pripade, ze se pridani clanku podarila.
     */
    public function addNewArticle(string $nazev, string $autor, string $pdf, string $abstrakt){
        $arguments = $this->replaceHTML($nazev, $autor, $abstrakt);
        $insertStatement = "autor, nazev, abstrakt, datum_vytvoreni, pdf, uzivatel_id";
        $datum_vytvoreni = date("Y-m-d H:i:s");
        $uzivatel_id = $this->session->readSession($this->userSessionKey);
        $insertValues = "'$arguments[1]', '$arguments[0]', '$arguments[2]', '$datum_vytvoreni', '$pdf', '$uzivatel_id'";
        return $this->insertIntoTable(TABLE_CLANEK, $insertStatement, $insertValues);
    }

    /**
     * Upravi clanek v databazi.
     * @param string $clanek_id         ID upravovaneho clanku.
     * @param string $nazev             Upraveny nazev clanku.
     * @param string $autor             Upraveny autor clanku.
     * @param string $abstrakt          Upraveny abstrakt clanku.
     * @return bool                     True v pripade, ze se uprava clanku podarila.
     */
    public function updateArticle(string $clanek_id, string $nazev, string $autor, string $abstrakt){
        $arguments = $this->replaceHTML($clanek_id, $nazev, $autor, $abstrakt);
        $updateStatementWithValues = "nazev='$arguments[1]', autor='$arguments[2]', abstrakt='$arguments[3]'";
        $whereStatement = "clanek_id=$arguments[0]";
        return $this->updateInTable(TABLE_CLANEK, $updateStatementWithValues, $whereStatement);
    }

    /**
     * V pripade, ze clanek neni schvalen, jej z databaze odstrani i s jeho prislusnimi recenzemi.
     * @param string $clanek_id
     * @return bool     True v pripade, ze se odstraneni podarilo.
     */
    public function deleteArticle(string $clanek_id){
        $arguments = $this->replaceHTML($clanek_id);
        $clanek = $this->selectFromTable(TABLE_CLANEK, "clanek_id='$arguments[0]'");
        if($clanek['schvalen'] == 0){
            $reviews = $this->selectFromTable(TABLE_RECENZE, "clanek_id='$arguments[0]'");
            foreach ($reviews as $review){
                $this->deleteReview($review['recenze_id']);
            }
            return $this->deleteFromTable(TABLE_CLANEK, "clanek_id='$arguments[0]'");
        } else {
            return false;
        }
    }

    /**
     * Prida novou recenzi do databaze.
     * @param string $recenzent_id      ID recenzenta.
     * @param string $clanek_id         ID clanku.
     * @return bool                     True v pripade, ze se pridani recenze podarilo.
     */
    public function addNewReview(string $recenzent_id, string $clanek_id){
        $arguments = $this->replaceHTML($recenzent_id, $clanek_id);
        $insertStatement = "datum, uzivatel_id, clanek_id";

        $datum = date("Y-m-d H:i:s");
        $insertValues = "'$datum', '$arguments[0]', '$arguments[1]'";

        return $this->insertIntoTable(TABLE_RECENZE, $insertStatement, $insertValues);
    }

    /**
     * Vrati recenze uzivatele a k nim prirazene clanky.
     * @return array    Pole recenzi a k nim prirazenych clanku.
     */
    public function getUserReviews(){
        $uzivatel_id = $this->session->readSession($this->userSessionKey);
        $reviews = $this->selectFromTable(TABLE_RECENZE, "uzivatel_id='$uzivatel_id'", "datum DESC");
        $whereStatement = "";
        $count = 0;
        foreach ($reviews as $review) {
            if($count == 0){
                $whereStatement .= "clanek_id='$review[clanek_id]'";
            } else {
                $whereStatement .= "OR clanek_id='$review[clanek_id]'";
            }
            $count++;
        }

        $articles = array();
        if($count != 0){
            $articles = $this->selectFromTable(TABLE_CLANEK, $whereStatement, "datum_vytvoreni DESC");
        }

        return array("reviews" => $reviews, "articles" => $articles);
    }

    /**
     * Odstrani recenzi z databaze.
     * @param string $recenze_id    ID odstranovane recenze.
     * @return bool                 True v pripade, ze se odstraneni recenze podarilo.
     */
    public function deleteReview(string $recenze_id){
        $arguments = $this->replaceHTML($recenze_id);
        return $this->deleteFromTable(TABLE_RECENZE, "recenze_id='$arguments[0]'");
    }

    /**
     * Vrati vsechny clanky prihlaseneho uzivatele.
     * @return array    Pole clanku prihlaseneho uzivatele.
     */
    public function getUserArticles(){
        $sql = "SELECT * FROM ".TABLE_CLANEK." WHERE uzivatel_id=?;";

        $uzivatel_id = $this->session->readSession($this->userSessionKey);

        $articles = $this->selectFromTableSafe($sql, array($uzivatel_id));
        return $articles;
    }

    /**
     * Vrati vsechny schvalene clanky.
     * @return array    Pole schvalenych clanku.
     */
    public function getAllAcceptedArticles(){
        $articles = $this->selectFromTable(TABLE_CLANEK, "schvalen='1'", "datum_vytvoreni DESC");
        return $articles;
    }

    /**
     * Vrati vsechny clanky, recenze a recenzenty.
     * @return array    Pole clanku, recenzi a recenzentu.
     */
    public function getAllArticlesWithReviews(){
        $articles = $this->selectFromTable(TABLE_CLANEK, "", "datum_vytvoreni DESC");
        $reviews = $this->selectFromTable(TABLE_RECENZE, "", "datum DESC");
        $reviewers = $this->selectFromTable(TABLE_UZIVATEL, "role_id=2");
        return array("articles" => $articles, "reviews" => $reviews, "reviewers" => $reviewers);
    }

    /**
     * Upravi recenzi v databazi.
     * @param string $recenze_id        ID upravovane recenze.
     * @param string $kvalita_obsahu    Kvalita obsahu upravovane recenze.
     * @param string $uroven            Uroven upravovane recenze.
     * @param string $novost            Novost upravovane recenze.
     * @param string $kvalita_jazyka    Kvalita jazyka upravovane recenze.
     * @param string $komentar          Komentar upravovane recenze.
     * @return bool                     True v pripade, ze se uprava recenze podarila.
     */
    public function updateReview(string $recenze_id, string $kvalita_obsahu, string $uroven, string $novost, string $kvalita_jazyka, string $komentar){
        $arguments = $this->replaceHTML($recenze_id, $kvalita_obsahu, $uroven, $novost, $kvalita_jazyka, $komentar);
        $updateStatementWithValues = "kvalita_obsahu='$arguments[1]', uroven='$arguments[2]', novost='$arguments[3]', kvalita_jazyka='$arguments[4]', komentar='$arguments[5]'";
        $whereStatement = "recenze_id=$arguments[0]";
        return $this->updateInTable(TABLE_RECENZE, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Vrati vsechny uzivatele z databaze.
     * @return array    Pole uzivatelu.
     */
    public function getAllUsers(){
        $users = $this->selectFromTable(TABLE_UZIVATEL, "", "role_id DESC");
        return $users;
    }

    /**
     * Vrati vsechny role v databazi.
     * @return array    Pole roli.
     */
    public function getAllRoles(){
        $rights = $this->selectFromTable(TABLE_ROLE, "", "nazev ASC");
        return $rights;
    }

    /**
     * Schvali dany clanek.
     * @param string $clanek_id     ID clanku.
     * @return bool                 True v pripade, ze se schvaleni clanku podarilo.
     */
    public function acceptArticle(string $clanek_id){
        $arguments = $this->replaceHTML($clanek_id);
        $updateStatementWithValues = "schvalen='1'";
        $whereStatement = "clanek_id=$arguments[0]";
        return $this->updateInTable(TABLE_CLANEK, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Vrati clanek k posouzeni.
     * @param string $clanek_id     ID clanku.
     * @return bool                 True v pripade, ze se vraceni clanku k posouzeni podari.
     */
    public function articleToReview(string $clanek_id){
        $arguments = $this->replaceHTML($clanek_id);
        $updateStatementWithValues = "schvalen='0'";
        $whereStatement = "clanek_id=$arguments[0]";
        return $this->updateInTable(TABLE_CLANEK, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Zamitne clanek.
     * @param string $clanek_id     ID clanku.
     * @return bool                 True v pripade, ze se zamitnuti clanku podari.
     */
    public function rejectArticle(string $clanek_id){
        $arguments = $this->replaceHTML($clanek_id);
        $updateStatementWithValues = "schvalen='2'";
        $whereStatement = "clanek_id=$arguments[0]";
        return $this->updateInTable(TABLE_CLANEK, $updateStatementWithValues, $whereStatement);
    }

    /**
     * Nahradi HTML znaky specialnimi znaky.
     * @param string ...$arguments      Argumenty na kontrolu.
     * @return array                    Pole zkonstrolovanych argumentu.
     */
    public function replaceHTML(string... $arguments){
        $replacedArgs = array();
        foreach ($arguments as $argument){
            array_push($replacedArgs, htmlspecialchars($argument));
        }
        return $replacedArgs;
    }
}
?>
