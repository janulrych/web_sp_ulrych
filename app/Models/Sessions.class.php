<?php

namespace websp\Models;

/**
 *  Trida pro praci se sessions.
 */
class Sessions{

    /**
     * Konstruktor tridy Sessions.
     * V pripade, ze session neni zahajena, tak ji zahaji.
     */
    public function __construct(){
        if(session_status() == PHP_SESSION_DISABLED || session_status() == PHP_SESSION_NONE){
            session_start();
        }
    }

    /**
     * Prida zpravu do session.
     * @param string $text      Text zpravy.
     * @param string $class     CSS trida zpravy.
     */
    public function addMessage(string $text, string $class){
        $message = array("text" => $text, "class" => $class);
        $this->addSession('message', $message);
    }

    /**
     * Zjisti, zda je nejaka aktivni zprava.
     * @return bool     True v pripade, ze je aktivni zprava.
     */
    public function isMessage(): bool{
        return $this->isSessionSet('message');
    }

    /**
     * Vrati aktivni zpravu.
     * @return mixed   Aktivni zprava.
     */
    public function getMessage(){
        $message = $this->readSession('message');
        $this->removeSession('message');
        return $message;
    }

    /**
     * Ulozi hodnotu do session.
     * @param string $name    Jmeno session.
     * @param mixed $value    Hodnota session.
     */
    public function addSession($name, $value){
        $_SESSION[$name] = $value;
    }

    /**
     * Vrati hodnotu pozadovane session.
     * @param string $name     Nazev session.
     * @return mixed           Hodnota session.
     */
    public function readSession($name){
        // existuje dany atribut v session
        if($this->isSessionSet($name)){
            return $_SESSION[$name];
        } else {
            return null;
        }
    }

    /**
     * Zjisti, zda je dana session nastavena.
     * @param string $name      Jmeno session.
     * @return boolean          True v pripade, ze session je nastavena.
     */
    public function isSessionSet($name){
        return isset($_SESSION[$name]);
    }

    /**
     *  Odstrani pozadovanou session.
     *  @param string $name     Jmeno session.
     */
    public function removeSession($name){
        unset($_SESSION[$name]);
    }
}
?>