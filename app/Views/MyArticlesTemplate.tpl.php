<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();

if(isset($_POST['upravit'])){
    if(isset($_POST['nazev']) && isset($_POST['autor']) && isset($_POST['abstrakt']) && isset($_POST['clanek_id'])){
        if($db->updateArticle($_POST['clanek_id'], $_POST['nazev'], $_POST['autor'], $_POST['abstrakt'])){
            $session->addMessage('Článek upraven', 'alert-success');
            header("Location: ?page=moje_clanky");
        } else {
            $session->addMessage('Nepodařilo se upravit článek', 'alert-danger');
            header("Location: ?page=moje_clanky");
        }
    } else {
        $session->addMessage('Nepodařilo se upravit článek', 'alert-danger');
        header("Location: ?page=moje_clanky");
    }
}

if(isset($_POST['smazat'])){
    if(isset($_POST['clanek_id'])){
        if($db->deleteArticle($_POST['clanek_id'])){
            $session->addMessage('Článek smazán', 'alert-success');
            header("Location: ?page=moje_clanky");
        } else {
            $session->addMessage('Nepodařilo se smazat článek', 'alert-danger');
            header("Location: ?page=moje_clanky");
        }
    } else {
        $session->addMessage('Nepodařilo se smazat článek', 'alert-danger');
        header("Location: ?page=moje_clanky");
    }
}

?>

<?php

$res = "";

if(count($tplData['articles']) > 0){
    foreach($tplData['articles'] as $a){
        $res .= "<div class='container shadow rounded border border-light p-2 mt-2 mb-2 w-75'>";
        if($a['schvalen'] == 2){
            $res .= "<div class='alert-danger rounded p-1 text-center'>Zamítnut</div>";
        } else if ($a['schvalen'] == 1){
            $res .= "<div class='alert-success rounded p-1 text-center'>Schválen</div>";
        } else if ($a['schvalen'] == 0){
            $res .= "<div class='alert-warning rounded p-1 text-center'>Čeká se na hodnocení</div>";
        }
        $filename = "uploads\\".$a['pdf'];
        if ($a['schvalen'] == 0){
            $res .= '
                    <form action="" method="post" enctype="multipart/form-data">
                        <label class="form-label" for="nazev">Název článku:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text fa fa-bars"></span>
                            </div>
                            <input class="form-control form-control-sm me-2" id="nazev" type="text" name="nazev" placeholder="Název článku" value="'.$a['nazev'].'">
                        </div>
                        
                        <label class="form-label" for="autor">Autor (autoři):</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text fa fa-users"></span>
                            </div>
                            <input class="form-control form-control-sm me-2" id="autor" type="text" name="autor" placeholder="Autor (autoři)" value="'.$a['autor'].'">
                        </div>
                         
                        <label class="form-label" for="abstrakt">Abstrakt:</label>
                        <textarea name="abstrakt" class="form-control form-control-sm me-2 font-monospace" id="abstrakt" rows="4"
                            placeholder="Abstrakt">'.$a['abstrakt'].'</textarea>
                        
                        <div class="d-flex justify-content-center mt-3">
                            <input type="hidden" name="clanek_id" value="'.$a['clanek_id'].'">
                            <button class="btn btn-sm btn-success" id="upravit" type="submit" name="upravit">Upravit</button>
                        </div> 
                    </form>
                    <form action="" method="post">
                        <div class="btn btn-primary" id="$a[clanek_id]_btn" onclick="showIFrame('.$a['clanek_id'].')">Zobrazit PDF</div>
                        <input type="hidden" name="clanek_id" value="'.$a['clanek_id'].'">
                        <button class="btn btn-danger" type="submit" name="smazat" value="smazat">Smazat článek</button>
                    </form>   
                    <iframe style="display: none" src="'.$filename.'" id="'.$a['clanek_id'].'" width="100%" height="500px"></iframe>
                </div>
            ';
        } else {
            $res .= "
            <h5>$a[autor]: $a[nazev]</h5>
            <p><strong>Abstrakt: </strong>$a[abstrakt]</p>
            <div class='btn btn-primary' id='$a[clanek_id]_btn' onclick='showIFrame($a[clanek_id])'>Zobrazit PDF</div>
            <iframe style='display: none' src='$filename' id='$a[clanek_id]' width='100%' height='500px'></iframe>
        </div>
        ";
        }
    }
} else {
    $res .= "
        <div class='container rounded p-5 m-auto bg-primary bg-opacity-50 shadow text-center'>
            <h3 class='m-auto'>Zatím jste nepublikoval žádné články...</h3>
            <div class='row mt-5 justify-content-center'>
                <p>Chcete publikovat nový článek?</p>
                <a href='?page=novy_clanek'>
                    <button class='btn btn-success'>Ano</button>
                </a>   
            </div>
        </div>
    ";
}

$res .= '<script src="js/show-pdf.js"></script>';

echo $res;

?>
