<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();
$um = new \websp\Models\UserModel();

if(isset($_POST['publikovat'])){
    if(isset($_POST['nazev']) && isset($_POST['autor']) && !empty($_FILES['file']['name']) && isset($_POST['abstrakt'])){
        $targetDir = "\\uploads\\";
        $fileName = 'user'.$um->getUserID() . '_' . date("Y-m-dTH.i.s") . '_' . basename($_FILES["file"]["name"]);
        $targetFilePath = $targetDir . $fileName;
        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

        if($fileType == 'pdf'){
            if(move_uploaded_file($_FILES['file']['tmp_name'], SITE_ROOT.$targetDir.$fileName)){
                if($db->addNewArticle($_POST['nazev'], $_POST['autor'], $fileName, $_POST['abstrakt'])){
                    $session->addMessage('Článek uložen', 'alert-success');
                    header("Location: ?page=moje_clanky");
                }
            } else {
                $session->addMessage('Nepodařilo se uložit článek', 'alert-danger');
                header("Location: ?page=novy_clanek");
            }
        } else {
            $session->addMessage('Nepodařilo se uložit článek', 'alert-danger');
            header("Location: ?page=novy_clanek");
        }
    } else {
        $session->addMessage('Nepodařilo se uložit článek', 'alert-danger');
        header("Location: ?page=novy_clanek");
    }
}

?>

<?php

echo '
    <div class="container mt-5 mb-5">
        <form action="" method="post" enctype="multipart/form-data">
            <label class="form-label" for="nazev">Název článku:</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text fa fa-bars"></span>
                </div>
                <input class="form-control form-control-sm me-2" id="nazev" type="text" name="nazev" placeholder="Název článku">
            </div>
            
            <label class="form-label" for="autor">Autor (autoři):</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text fa fa-users"></span>
                </div>
                <input class="form-control form-control-sm me-2" id="autor" type="text" name="autor" placeholder="Autor (autoři)">
            </div> 
             
            <label class="form-label" for="pdf">PDF:</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text fa fa-file"></span>
                </div>
                <input class="form-control form-control-sm me-2" id="file" type="file" accept="application/pdf" name="file" placeholder="PDF soubor">
            </div> 
             
            <label class="form-label" for="abstrakt">Abstrakt:</label>
            <textarea name="abstrakt" class="form-control form-control-sm me-2 font-monospace" id="abstrakt" rows="4"
                placeholder="Abstrakt"></textarea>
            
            <div class="d-flex justify-content-center mt-3">
                <button class="btn btn-sm btn-success" id="publikovat" type="submit" name="publikovat">Publikovat</button>
            </div> 
        </form>
    </div>
';

?>
