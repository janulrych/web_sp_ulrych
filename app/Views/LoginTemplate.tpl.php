<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();
$um = new \websp\Models\UserModel();

if(isset($_POST['prihlasit'])){
    if(isset($_POST['login']) && isset($_POST['heslo'])){
        if($um->userLogin($_POST['login'], $_POST['heslo'])){
            $user = $um->getLoggedUserData();
            if($user['zablokovan'] == 0){
                $session->addMessage('Byl jste přihlášen', 'alert-success');
                header("Location: ?page=uvod");
            } else if($user['zablokovan'] == 1){
                $session->addMessage('Váš účet je zablokován, nemůžeme Vás přihlásit', 'alert-danger');
                $um->userLogout();
                header("Location: ?page=uvod");
            }
        } else {
            $session->addMessage('Nesprávné údaje', 'alert-danger');
            header("Location: ?page=login");
        }
    } else {
        $session->addMessage('Nesprávné údaje', 'alert-danger');
        header("Location: ?page=login");
    }
}

?>

<?php

echo '
    <div class="d-flex justify-content-center mt-5 mb-5">
        <div class="card shadow">
            <div class="card-header">
                <span class="fw-bold">Přihlášení</span>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <label class="form-label" for="login">Přihlašovací jméno:</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text fa fa-user"></span>
                        </div>
                        <input class="form-control form-control-sm me-2" id="login" type="text" name="login" placeholder="vaše přihlašovací jméno">
                    </div>

                    <label class="form-label" for="password">Heslo:</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text fa fa-key"></span>
                        </div>
                        <input class="form-control form-control-sm me-2" id="password" type="password" name="heslo" placeholder="vaše heslo">
                    </div>
                    <div class="d-flex justify-content-center mt-3">
                        <button class="btn btn-sm btn-success" type="submit" name="prihlasit">Přihlásit se</button>
                    </div>

                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-around">
                    <a href="?page=registration" target="_self">
                        <button class="btn btn-sm btn-primary">Registrace</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
';

?>
