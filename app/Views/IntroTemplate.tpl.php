<?php

global $tplData;

$res = "
<div class='container rounded p-5 mt-2 bg-primary bg-opacity-50 shadow text-center'>
    <h1 class='m-auto'>KONFERENCE VĚTRÁK 2021</h1>
</div>
<div class='container rounded p-5 mt-2 mb-2 bg-light shadow'>
    <h1 class='m-auto text-center mb-2'>Úvodní slovo</h1>
    <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta posuere lacus id consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porttitor purus arcu, rutrum sollicitudin turpis sodales sit amet. Quisque gravida ante sed risus feugiat, non auctor neque sollicitudin. Proin eleifend pretium ligula, eu scelerisque diam vehicula et. Phasellus vulputate dolor sed lacus sagittis sollicitudin. Morbi cursus, purus vel consequat tempus, tellus sem lobortis lorem, a eleifend lorem nisl ut magna. Quisque tempor blandit leo sit amet fermentum. Aenean tristique dui sit amet velit cursus, vel rhoncus orci vulputate. In vestibulum pharetra urna. Nam tempus ex ac dui lacinia, id fringilla tortor cursus. Etiam faucibus metus leo, vel gravida velit vulputate vel. Praesent malesuada, ipsum condimentum finibus elementum, felis leo posuere leo, ut bibendum risus dui non velit.
    </p>
    <p>
    Ut non pulvinar urna. Duis vehicula ornare maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mollis magna quis est lacinia, sit amet tempus felis ornare. Maecenas ullamcorper semper neque, vel convallis velit. Vivamus maximus commodo viverra. Maecenas mauris ex, feugiat at dui nec, molestie dictum elit. Suspendisse semper orci ut felis venenatis, sed faucibus odio finibus. Vestibulum et mauris et dui pellentesque lobortis. Donec vel ligula semper ipsum hendrerit pellentesque.
    </p>
    <p>
    Nulla pulvinar vehicula odio, et finibus leo efficitur eget. Duis vehicula arcu placerat arcu ultrices, vel venenatis dolor lacinia. Nullam euismod in nulla non tempus. Nullam molestie ornare nisl, non ultricies velit dignissim ut. Praesent in nunc scelerisque, euismod est ultrices, eleifend nibh. In sem erat, lacinia sit amet mauris vitae, egestas lobortis nisl. Morbi id purus vitae odio rhoncus laoreet. Donec fermentum iaculis quam eu varius.
    </p>
    <p>
    Curabitur blandit tempor lectus posuere tincidunt. Maecenas rhoncus ipsum eget risus tristique pretium. Vivamus in ornare ipsum. Donec vitae eros bibendum, commodo ex in, semper diam. Vivamus viverra, diam eget molestie congue, risus nulla varius lacus, id consectetur est sem at velit. Morbi semper enim ipsum, id commodo nulla pretium eget. Donec fringilla enim in orci interdum, vel consectetur dui blandit.
    </p>
    <p>
    Nulla facilisi. Integer justo ligula, cursus eu porta nec, tempor id turpis. Sed fermentum ipsum lacus, ac placerat justo pretium ac. Ut velit ante, interdum a aliquam sit amet, tempus nec tellus. Aliquam ut egestas justo. Etiam enim enim, vehicula a odio quis, aliquet mattis mi. Proin a finibus felis. Ut volutpat dolor nec quam placerat, sed elementum dolor posuere. Duis posuere, diam sed condimentum sodales, quam justo ornare orci, pretium elementum risus felis non mauris. Aliquam tempor nisl a risus venenatis mollis. Curabitur convallis erat id dui placerat, sed pharetra justo luctus. Nullam venenatis enim quis rhoncus posuere. Vestibulum interdum erat quis porttitor suscipit. Nunc fringilla leo quis diam facilisis egestas. Donec ut hendrerit libero. Proin consectetur suscipit odio, mollis pharetra mi maximus vitae.
    </p>
    <div class='text-end'>
        <p><strong>Váš tým Konference Větrák 2021</strong></p>
    </div>
    
</div>
";
echo $res;

?>
