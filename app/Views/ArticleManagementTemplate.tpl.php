<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();

if(isset($_POST['odeslat'])){
    if(isset($_POST['recenzent']) && isset($_POST['clanek_id'])){
        if($db->addNewReview($_POST['recenzent'], $_POST['clanek_id'])){
            $session->addMessage('Recenze přidána', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se přidat recenzi', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se přidat recenzi', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

if(isset($_POST['delete'])){
    if(isset($_POST['recenze_id'])){
        if($db->deleteReview($_POST['recenze_id'])){
            $session->addMessage('Recenze odebrána', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se odebrat recenzi', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se odebrat recenzi', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

if(isset($_POST['schvalit'])){
    if(isset($_POST['clanek_id'])){
        if($db->acceptArticle($_POST['clanek_id'])){
            $session->addMessage('Článek schválen', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se schválit článek', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se schválit článek', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

if(isset($_POST['zamitnout'])){
    if(isset($_POST['clanek_id'])){
        if($db->rejectArticle($_POST['clanek_id'])){
            $session->addMessage('Článek zamítnut', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se zamítnout článek', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se zamítnout článek', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

if(isset($_POST['review'])){
    if(isset($_POST['clanek_id'])){
        if($db->articleToReview($_POST['clanek_id'])){
            $session->addMessage('Článek vrácen ke schválení', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se vrátit článek ke schválení', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se vrátit článek ke schválení', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

if(isset($_POST['delete_article'])){
    if(isset($_POST['clanek_id'])){
        if($db->deleteArticle($_POST['clanek_id'])){
            $session->addMessage('Článek smazán', 'alert-success');
            header( "Location: ?page=sprava_clanku" );
        } else {
            $session->addMessage('Nepodařilo se smazat článek', 'alert-danger');
            header( "Location: ?page=sprava_clanku" );
        }
    } else {
        $session->addMessage('Nepodařilo se smazat článek', 'alert-danger');
        header( "Location: ?page=sprava_clanku" );
    }
}

?>

<?php

$res = "";

if(count($tplData['data']['articles']) > 0) {
    foreach ($tplData['data']['articles'] as $a) {
        $res .= "<div class='container shadow rounded border border-light p-2 mt-4 mb-4 w-75'>";
        if ($a['schvalen'] == 2) {
            $res .= "<div class='alert-danger rounded p-1 text-center'>Zamítnut</div>";
        } else if ($a['schvalen'] == 1) {
            $res .= "<div class='alert-success rounded p-1 text-center'>Schválen</div>";
        } else if ($a['schvalen'] == 0) {
            $res .= "<div class='alert-warning rounded p-1 text-center'>Čeká se na hodnocení</div>";
        }
        $res .= "
            <h5>$a[autor]: $a[nazev]</h5>
            <p>$a[datum_vytvoreni]</p>
        
    ";

        if ($a['schvalen'] == 0) {
            $res .= "<form action='' method='post'>
            <label for='recenzent'>Přidat recenzenta:</label>
            <select id='recenzent' name='recenzent'>";
            foreach ($tplData['data']['reviewers'] as $reviewer) {
                if ($reviewer['zablokovan'] == 1) continue;
                $res .= "
                <option value='$reviewer[uzivatel_id]'>$reviewer[jmeno] $reviewer[prijmeni]</option>
            ";
            }

            $res .= "</select>
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button type='submit' class='btn btn-sm btn-success' name='odeslat' value='odeslat'>Přidat</button>
            </form>
        ";
        }

        $recenze = array();
        foreach ($tplData['data']['reviews'] as $r) {
            if ($r['clanek_id'] == $a['clanek_id']) {
                array_push($recenze, $r);
            }
        }

        if (count($recenze) > 0) {
            $res .= "
    <table class='table'>
        <thead>
            <tr>
                <th>Recenzent</th>
                <th>Kvalita obsahu</th>
                <th>Úroveň</th>
                <th>Novost</th>
                <th>Kvalita jazyka</th>
                <th>Komentář</th>
                <th>Datum</th>
            </tr>
        </thead>
        <tbody>";

            foreach ($recenze as $r) {
                $resRew = "";
                foreach ($tplData['data']['reviewers'] as $reviewer) {
                    if ($reviewer['uzivatel_id'] == $r['uzivatel_id']) {
                        $resRew = $reviewer['jmeno'] . " " . $reviewer['prijmeni'];
                    }
                }
                $res .= "
        <tr>
            <td>$resRew</td>
            <td>$r[kvalita_obsahu]</td>
            <td>$r[uroven]</td>
            <td>$r[novost]</td>
            <td>$r[kvalita_jazyka]</td>
            <td>$r[komentar]</td>
            <td>$r[datum]</td>"
                    . "<td>
                <form method='post'>"
                    . "<input type='hidden' name='recenze_id' value='$r[recenze_id]'>"
                    . "<button class='btn btn-sm btn-danger' type='submit' name='delete' value='delete'>Smazat</button>"
                    . "</form>
          </td>
        </tr>";
            }
            $res .= "
                </tbody>
            </table>";
        }

        $res .= "<form class='mt-2' action='' method='post'>";

        if ($a['schvalen'] == 0) {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-danger' type='submit' name='delete_article' value='delete_article'>Smazat článek</button>
            <button class='btn btn-sm btn-warning' type='submit' name='review' value='review' disabled>Vrátit ke schválení</button>
        ";
        } else {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-sm btn-warning' type='submit' name='review' value='review'>Vrátit ke schválení</button>
        ";
        }

        if ($a['schvalen'] == 1) {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-sm btn-success' type='submit' name='schvalit' value='schvalit' disabled>Schválit</button>
        ";
        } else {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-sm btn-success' type='submit' name='schvalit' value='schvalit'>Schválit</button>
        ";
        }

        if ($a['schvalen'] == 2) {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-sm btn-danger' type='submit' name='zamitnout' value='zamitnout' disabled>Zamítnout</button>
        ";
        } else {
            $res .= "
            <input type='hidden' name='clanek_id' value='$a[clanek_id]'>
            <button class='btn btn-sm btn-danger' type='submit' name='zamitnout' value='zamitnout'>Zamítnout</button>
        ";
        }

        $res .= "</form>";
        $res .= "</div>";

    }
} else {
    $res .= "
        <div class='container rounded p-5 m-auto bg-primary bg-opacity-50 shadow text-center'>
            <h3 class='m-auto'>Zatím nebyl napsán žádný článek...</h3>
        </div>
    ";
}

$res .= '<script src="js/show-pdf.js"></script>';

echo $res;

?>
