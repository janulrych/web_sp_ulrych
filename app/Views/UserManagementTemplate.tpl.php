<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();
$um = new \websp\Models\UserModel();

if(isset($_POST['block'])){
    if($um->blockUser($_POST['uzivatel_id'])){
        $session->addMessage('Uživatel zablokován', 'alert-success');
        header("Location: ?page=uzivatele");
    } else {
        $session->addMessage('Zablokování uživatele se nepodařilo', 'alert-danger');
        header("Location: ?page=uzivatele");
    }
}

if(isset($_POST['unblock'])){
    if($um->unblockUser($_POST['uzivatel_id'])){
        $session->addMessage('Uživatel odblokován', 'alert-success');
        header("Location: ?page=uzivatele");
    } else {
        $session->addMessage('Odblokování uživatele se nepodařilo', 'alert-danger');
        header("Location: ?page=uzivatele");
    }
}

if(isset($_POST['change_role'])){
    if($um->updateUserRole($_POST['uzivatel_id'], $_POST['role_id'])){
        $session->addMessage('Role uživatele změněna', 'alert-success');
        header("Location: ?page=uzivatele");
    } else {
        $session->addMessage('Nepodařilo se změnit roli uživatele', 'alert-danger');
        header("Location: ?page=uzivatele");
    }
}

?>

<?php

$res = "
<div class='container w-75 shadow mt-5 mb-2'>
<table class='table'>
    <thead>
        <tr>
            <th>ID</th>
            <th>Jméno</th>
            <th>Příjmení</th>
            <th>Login</th>
            <th>E-mail</th>
            <th>Adresa</th>
            <th>Role</th>
            <th>Smazat</th>
        </tr>
    </thead>
    <tbody>";
$adminRole = $um->getUserRole();

$options = "";
foreach ($tplData['roles'] as $r){
    if($r['role_id'] < $adminRole){
        $options .= "<option value='$r[role_id]'>$r[nazev]</option>";
    }
}

foreach($tplData['users'] as $u){
    $res .= "
        <tr>
            <td>$u[uzivatel_id]</td>
            <td>$u[jmeno]</td>
            <td>$u[prijmeni]</td>
            <td>$u[login]</td>
            <td>$u[email]</td>
            <td><textarea cols='11' rows='2' placeholder='$u[adresa]' disabled></textarea></td>";
    if($u['role_id'] < $adminRole){
        $options = "";
        foreach ($tplData['roles'] as $r){
            if($r['role_id'] < $adminRole){
                if($u['role_id'] == $r['role_id']){
                    $options .= "<option value='$r[role_id]' selected>$r[nazev]</option>";
                } else {
                    $options .= "<option value='$r[role_id]'>$r[nazev]</option>";
                }
            }
        }
        $res .= "<td style='min-width: 5rem'>
                <form action='' method='post'>
                    <div class='row justify-content-center'>
                        <select name='role_id'>"
                            . $options .
                        "</select>
                        <input type='hidden' name='uzivatel_id' value='$u[uzivatel_id]'>
                        <button class='btn btn-sm btn-warning' type='submit' name='change_role' value='change_role'>Změnit</button>
                    </div>
                </form>
            </td>";

        if($u['zablokovan'] == 0){
            $res .= "<td>
                <form action='' method='post'>
                    <input type='hidden' name='uzivatel_id' value='$u[uzivatel_id]'>
                    <button class='btn btn-sm btn-danger' type='submit' name='block' value='block'>Zablokovat</button>
                </form>
            </td>";
        } else if($u['zablokovan'] == 1){
            $res .= "<td>
                <form action='' method='post'>
                    <input type='hidden' name='uzivatel_id' value='$u[uzivatel_id]'>
                    <button class='btn btn-sm btn-success' type='submit' name='unblock' value='unblock'>Odblokovat</button>
                </form>
            </td>";
        }

    } else {
        $res .= "<td></td>
            <td><button class='btn btn-sm btn-danger' disabled>Zablokovat</button></td>
        ";
    }

    $res .= "</tr>";
}

$res .= "
    </tbody>
</table></div>";
echo $res;

?>
