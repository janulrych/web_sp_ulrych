<?php

global $tplData;
$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();
$um = new \websp\Models\UserModel();

if(isset($_POST['upravit'])){
    if(isset($_POST['uzivatel_id']) && isset($_POST['jmeno']) && isset($_POST['prijmeni']) && isset($_POST['email']) && isset($_POST['adresa'])){
        $res = $um->updateUser($_POST['uzivatel_id'], $_POST['jmeno'], $_POST['prijmeni'], $_POST['email'], $_POST['adresa']);
        if($res){
            $session->addMessage('Profil upraven', 'alert-success');
            header("Location: ?page=muj_profil");
        } else {
            $session->addMessage('Úprava profilu se nezdařila', 'alert-danger');
            header("Location: ?page=muj_profil");
        }
    } else {
        $session->addMessage('Nebyly vyplněny nějaké údaje', 'alert-danger');
        header("Location: ?page=muj_profil");
    }
}

if(isset($_POST['zmenit'])){
    if(isset($_POST['uzivatel_id']) && isset($_POST['heslo']) && isset($_POST['heslo_nove1']) && isset($_POST['heslo_nove2']) && $_POST['heslo_nove1'] == $_POST['heslo_nove2']){
        $user = $um->getLoggedUserData();
        if(password_verify($_POST['heslo'], $user['heslo'])){
            $password = password_hash($_POST['heslo_nove1'], null);
            if($um->updateUserPassword($_POST['uzivatel_id'], $password)){
                $session->addMessage('Heslo bylo úspěšně změněno, nyní se, prosím, znovu přihlašte', 'alert-success');
                $um->userLogout();
                header("Location: ?page=uvod");
            } else {
                $session->addMessage('Změna hesla se nezdařila', 'alert-danger');
                header("Location: ?page=muj_profil");
            }
        } else {
            $session->addMessage('Změna hesla se nezdařila', 'alert-danger');
            header("Location: ?page=muj_profil");
        }
    } else {
        $session->addMessage('Změna hesla se nezdařila', 'alert-danger');
        header("Location: ?page=muj_profil");
    }
}

?>

<?php

$user = $tplData['user'];

echo '
<div class="d-flex justify-content-center mt-3">
    <div class="card shadow">
        <div class="card-header">
            <span class="fw-bold">Moje údaje</span>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <label class="form-label" for="login">Přihlašovací jméno:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="login" class="form-control form-control-sm me-2" id="login" type="text"
                           placeholder="např. jmeno_prijmeni" value="'.$user['login'].'" onkeyup="checkFill()" disabled>
                </div>

                <label class="form-label" for="email">E-mail:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-envelope"></span>
                    </div>
                    <input name="email" class="form-control form-control-sm me-2" id="email" type="email"
                           placeholder="priklad@email.cz" value="'.$user['email'].'" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="name">Jméno:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="jmeno" class="form-control form-control-sm me-2" id="name" type="text"
                           placeholder="např. Jan" value="'.$user['jmeno'].'" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="surname">Příjmení:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="prijmeni" value="'.$user['prijmeni'].'" class="form-control form-control-sm me-2" id="surname" type="text"
                           placeholder="např. Novák" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="address">Korespondenční adresa:</label>
                <textarea name="adresa" class="form-control form-control-sm me-2 font-monospace" id="address" rows="4"
                    placeholder="Jméno Příjmení\nUlice č.p.\nMěsto\nPSČ" onkeyup="checkFill()">'.$user['adresa'].'</textarea>
                <div class="d-flex justify-content-center mt-3">
                    <input type="hidden" name="uzivatel_id" value="'.$user['uzivatel_id'].'">
                    <button class="btn btn-sm btn-success" id="upravit" type="submit" name="upravit" disabled>Upravit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center mt-3 mb-3">
    <div class="card shadow">
        <div class="card-header">
            <span class="fw-bold">Změnit heslo</span>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <label class="form-label" for="heslo">Vaše stávající heslo:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-key"></span>
                    </div>
                    <input name="heslo" class="form-control form-control-sm me-2" type="password"
                           placeholder="např. 12345heslo">
                </div>
                
                <label class="form-label" for="heslo_nove1">Nové heslo:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-key"></span>
                    </div>
                    <input name="heslo_nove1" class="form-control form-control-sm me-2" type="password"
                           placeholder="např. heslo12345">
                </div>

                <label class="form-label" for="heslo_nove2">Nové heslo podruhé:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-key"></span>
                    </div>
                    <input name="heslo_nove2" class="form-control form-control-sm me-2" type="password"
                           placeholder="např. heslo12345">
                </div>
                
                <div class="d-flex justify-content-center mt-3">
                    <input type="hidden" name="uzivatel_id" value="'.$user['uzivatel_id'].'">
                    <button class="btn btn-sm btn-success" type="submit" name="zmenit">Změnit heslo</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="js/text-area-placeholder.js"></script>
<script src="js/form-check/check-fill-my-profile.js"></script>
<script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
';

?>
