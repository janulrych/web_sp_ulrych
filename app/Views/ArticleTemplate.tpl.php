<?php

global $tplData;

$res = "";

if(count($tplData['articles']) > 0) {
    foreach ($tplData['articles'] as $a) {
        $res .= "<div class='container shadow rounded border border-light p-2 mt-4 w-75'>";

        $filename = "uploads\\" . $a['pdf'];
        $res .= "
            <h5>$a[autor]: $a[nazev]</h5>
            <p><strong>Abstrakt: </strong>$a[abstrakt]</p>
            <div class='btn btn-primary' id='$a[clanek_id]_btn' onclick='showIFrame($a[clanek_id])'>Zobrazit PDF</div>
            <iframe style='display: none' src='$filename' id='$a[clanek_id]' width='100%' height='500px'></iframe>
        </div>
    ";
    }
} else {
    $res .= "
        <div class='container rounded p-5 m-auto bg-primary bg-opacity-50 shadow text-center'>
            <h3 class='m-auto'>Zatím nebyl publikován žádný článek...</h3>
        </div>
    ";
}

$res .= '<script src="js/show-pdf.js"></script>';

echo $res;

?>
