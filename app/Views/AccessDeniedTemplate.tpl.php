<?php

global $tplData;

$res = "
<div class='container rounded p-5 m-auto bg-primary bg-opacity-50 shadow text-center'>
    <h3 class='m-auto'>Omlouváme se, ale sem nemáte přístup.</h3>
    <div class='row mt-5 justify-content-center'>
        <p>Zapomněl jste se přihlásit?</p>
        <a href='?page=login'>
            <button class='btn btn-success'>Přihlásit se</button>
        </a>   
    </div>
</div>
";

echo $res;

?>
