<?php

namespace websp\Views;

use websp\Models\DatabaseModel;
use websp\Models\Sessions;
use websp\Models\UserModel;

/**
 * Trida vypisujici HTML hlavicku a paticku stranky.
 * @package kivweb\Views
 */
class BaseTemplate implements IView{

    /** @var string PAGE_INTRODUCTION  Sablona pro uvodni stranku. */
    const PAGE_INTRODUCTION = "IntroTemplate.tpl.php";
    /** @var string PAGE_USER_MANAGEMENT  Sablona pro psravu uzivatelu. */
    const PAGE_USER_MANAGEMENT = "UserManagementTemplate.tpl.php";
    /** @var string PAGE_LOGIN  Sablona pro prihlaseni uzivatelu. */
    const PAGE_LOGIN = "LoginTemplate.tpl.php";
    /** @var string PAGE_REGISTRATION  Sablona pro registraci uzivatelu. */
    const PAGE_REGISTRATION = "RegistrationTemplate.tpl.php";
    /** @var string PAGE_NEW_ARTICLE  Sablona pro vytvoreni clanku. */
    const PAGE_NEW_ARTICLE = "NewArticleTemplate.tpl.php";
    /** @var string PAGE_MY_ARTICLES  Sablona pro zobrazeni clanku prihlaseneho uzivatele. */
    const PAGE_MY_ARTICLES = "MyArticlesTemplate.tpl.php";
    /** @var string PAGE_ARTICLE_MANAGEMENT  Sablona pro spravu clanku. */
    const PAGE_ARTICLE_MANAGEMENT = "ArticleManagementTemplate.tpl.php";
    /** @var string PAGE_MY_REVIEWS  Sablona pro zobrazeni recenzi prihlaseneho uzivatele. */
    const PAGE_MY_REVIEWS = "MyReviewsTemplate.tpl.php";
    /** @var string PAGE_ARTICLE  Sablona pro zobrazeni schvalenych clanku. */
    const PAGE_ARTICLE = "ArticleTemplate.tpl.php";
    /** @var string PAGE_MY_PROFILE  Sablona pro spravu profilu uzivatele. */
    const PAGE_MY_PROFILE = "MyProfileTemplate.tpl.php";
    /** @var string PAGE_ACCESS_DENIED  Sablona pro chybovou stranku (neprihlaseny uzivatel). */
    const PAGE_ACCESS_DENIED = "AccessDeniedTemplate.tpl.php";
    /** @var string PAGE_INSUFFICIENT_RIGHTS  Sablona pro chybovou stranku (nedostatecna prava). */
    const PAGE_INSUFFICIENT_RIGHTS = "InsufficientRightsTemplate.tpl.php";

    /** @var DatabaseModel $db  Databazovy model. */
    private $db;
    /** @var Sessions  Prace se sessions. */
    private $session;
    /** @var UserModel $um  Model pro praci s uzivatelem. */
    private $um;

    /**
     * Konstruktor tridy BaseTemplate.
     * Nainicializuje tridni promenne.
     */
    function __construct(){
        $this->db = new DatabaseModel();
        $this->session = new Sessions();
        $this->um = new UserModel();
    }

    /**
     * Zajisti vypsani HTML sablony prislusne stranky.
     * Pripadne vypise oznameni.
     * @param array $templateData       Data stranky.
     * @param string $pageType          Typ vypisovane stranky.
     */
    public function printOutput(array $templateData, string $pageType = self::PAGE_INTRODUCTION){
        $this->getHTMLHeader($templateData['title']);

        global $tplData;
        $tplData = $templateData;

        if($this->session->isMessage()){
            $message = $this->session->getMessage();
            echo "
                <div class='container w-50'>
                    <div class='alert $message[class] mt-2'>$message[text]</div>
                </div>
            ";
        }
        require_once($pageType);

        $this->getHTMLFooter();
    }


    /**
     * Vrati hlavicku stranky.
     * Pripadne odhlasi uzivatele.
     * @param string $pageTitle    Titulek stranky.
     */
    public function getHTMLHeader(string $pageTitle){
        ?>

        <?php
        if(isset($_POST['odhlasit'])){
            $this->um->userLogout();
        }
        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link rel="stylesheet" href="utils/font-awesome-4.7.0/css/font-awesome.css">
            <link rel="stylesheet" href="utils/node_modules/bootstrap/dist/css/bootstrap.min.css">
            <link rel="stylesheet" href="styles.css">

            <title><?php echo $pageTitle?> | Konference Větrák 2021</title>
        </head>

        <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
            <div class="container-fluid">
                <a class="text-decoration-none" href="?page=uvod">
                    <img src="assets/logo.gif" class="shadow" alt="logo" style="height: 35px; background-color: midnightblue; border: solid black 0px; border-radius: 100px;"/>
                    <span class="fw-bold text-black">KONFERENCE VĚTRÁK</span>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="?page=clanky">Články</a>
                        </li>
                        <?php
                        if($this->um->isUserLogged() && $this->um->getLoggedUserData()['role_id'] == 1){
                            echo '
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="?page=novy_clanek">Nový článek</a>
                                </li>
                            ';
                        }
                        ?>
                        <?php
                        if($this->um->isUserLogged() && $this->um->getLoggedUserData()['role_id'] >= 3){
                            echo '
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="?page=sprava_clanku">Správa článků</a>
                                </li>
                            ';
                        }
                        ?>
                        <?php
                        if($this->um->isUserLogged() && $this->um->getLoggedUserData()['role_id'] == 2){
                            echo '
                                <li class="nav-item">
                                    <a class="nav-link" href="?page=moje_recenze">Moje recenze</a>
                                </li>
                            ';
                        }
                        ?>
                        <?php
                        if($this->um->isUserLogged() && $this->um->getLoggedUserData()['role_id'] >= 3){
                            echo '
                                <li class="nav-item">
                                    <a class="nav-link" href="?page=uzivatele">Uživatelé</a>
                                </li>
                            ';
                        }
                        ?>
                        <?php
                        if($this->um->isUserLogged() && $this->um->getLoggedUserData()['role_id'] == 1){
                            echo '
                                <li class="nav-item">
                                    <a class="nav-link" href="?page=moje_clanky">Moje články</a>
                                </li>
                            ';
                        }
                        ?>
                    </ul>

                    <?php
                    if(!$this->um->isUserLogged()){
                        echo '
                            <a href="?page=login" target="_self">
                                <button class="btn btn-light shadow">
                                    Přihlášení
                                </button>
                            </a>
                        ';
                    } else {
                        echo '
                            <a href="?page=muj_profil" style="margin-right: 0.5em" target="_self">
                                <button class="btn btn-outline-light shadow">
                                    Můj profil
                                </button>
                            </a>
                            <form action="?page=uvod" method="post">
                                <button class="btn btn-light shadow" type="submit" name="odhlasit">
                                    Odhlásit se
                                </button>
                            </form>                         
                        ';
                    }
                    ?>

                </div>
            </div>
        </nav>
        <?php
    }

    /**
     * Vrati paticku stranky.
     */
    public function getHTMLFooter(){
        ?>
        <footer class="container-fluid text-center fw-bold shadow">
            <span class="fa fa-copyright"></span> Jan Ulrych,
            <script>
                document.write(" " + new Date().getFullYear());
            </script>
        </footer>

        <script type="text/javascript" src="utils/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

        <body>
        </html>
        <?php
    }
}

?>
