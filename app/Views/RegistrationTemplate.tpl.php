<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();
$um = new \websp\Models\UserModel();

if(isset($_POST['odeslat'])){
    if(!$um->userLoginExists($_POST['login'])){
        if(isset($_POST['jmeno']) && isset($_POST['prijmeni']) && isset($_POST['login'])
            && isset($_POST['heslo1']) && isset($_POST['heslo2']) && isset($_POST['email'])
            && isset($_POST['adresa']) && $_POST['heslo1'] == $_POST['heslo2']){

            $password = password_hash($_POST['heslo1'], null);

            if($um->addNewUser($_POST['jmeno'], $_POST['prijmeni'], $_POST['login'], $password, $_POST['email'], $_POST['adresa'])){
                $session->addMessage('Registrace proběhla úspěšně, můžete se přihlásit.', 'alert-success');
                header("Location: ?page=login");
            } else {
                $session->addMessage('Registrace se nezdařila.', 'alert-danger');
                header("Location: ?page=registration");
            }
        } else {
            $session->addMessage('Registrace se nezdařila.', 'alert-danger');
            header("Location: ?page=registration");
        }
    } else {
        $session->addMessage('Registrace se nezdařila.', 'alert-danger');
        header("Location: ?page=registration");
    }
}

?>

<?php

echo '
<div class="d-flex justify-content-center mt-5 mb-5">
    <div class="card shadow">
        <div class="card-header">
            <span class="fw-bold">Registrace</span>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <label class="form-label" for="login">Přihlašovací jméno:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="login" class="form-control form-control-sm me-2" id="login" type="text"
                           placeholder="např. jmeno_prijmeni" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="email">E-mail:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-envelope"></span>
                    </div>
                    <input name="email" class="form-control form-control-sm me-2" id="email" type="email"
                           placeholder="priklad@email.cz" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="password">Heslo zadejte pro kontrolu dvakrát:</label>
                <div class="input-group mb-2" id="firstInputField">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-key"></span>
                    </div>
                    <input name="heslo1" class="form-control form-control-sm me-2" id="password-first" type="password"
                           placeholder="např. heslo12345" onkeyup="checkPassword(); checkFill()">
                </div>
                <div class="input-group mb-2" id="secondInputField">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-key"></span>
                    </div>
                    <input name="heslo2" class="form-control form-control-sm me-2" id="password-second" type="password"
                           placeholder="např. heslo12345" onkeyup="checkPassword(); checkFill()">
                </div>

                <label class="form-label" for="name">Jméno:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="jmeno" class="form-control form-control-sm me-2" id="name" type="text"
                           placeholder="např. Jan" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="surname">Příjmení:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-user"></span>
                    </div>
                    <input name="prijmeni" class="form-control form-control-sm me-2" id="surname" type="text"
                           placeholder="např. Novák" onkeyup="checkFill()">
                </div>

                <label class="form-label" for="address">Korespondenční adresa:</label>
                <textarea name="adresa" class="form-control form-control-sm me-2 font-monospace" id="address" rows="4"
                    placeholder="Jméno Příjmení\nUlice č.p.\nMěsto\nPSČ" onkeyup="checkFill()"></textarea>
                <div class="d-flex justify-content-center mt-3">
                    <button class="btn btn-sm btn-success" id="register" type="submit" name="odeslat" disabled>Zaregistrovat</button>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-around">
                <a href="?page=login" target="_self">
                    <button class="btn btn-sm btn-primary">Přihlášení</button>
                </a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="js/text-area-placeholder.js"></script>
<script type="text/javascript" src="js/form-check/check-password.js"></script>
<script type="text/javascript" src="js/form-check/check-fill.js"></script>
<script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
';

?>
