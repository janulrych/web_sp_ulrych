<?php

global $tplData;

$db = new \websp\Models\DatabaseModel();
$session = new \websp\Models\Sessions();

if(isset($_POST['odeslat'])){
    if(isset($_POST['recenze_id']) && isset($_POST['kvalita_obsahu']) && isset($_POST['uroven']) && isset($_POST['novost'])  && isset($_POST['kvalita_jazyka']) && isset($_POST['komentar'])){
        if($db->updateReview($_POST['recenze_id'], $_POST['kvalita_obsahu'], $_POST['uroven'], $_POST['novost'], $_POST['kvalita_jazyka'], $_POST['komentar'])){
            $session->addMessage('Recenze uložena', 'alert-success');
            header("Location: ?page=moje_recenze");
        } else {
            $session->addMessage('Nepodařilo se uložit recenzi', 'alert-danger');
            header("Location: ?page=moje_recenze");
        }
    } else {
        $session->addMessage('Nepodařilo se uložit recenzi', 'alert-danger');
        header("Location: ?page=moje_recenze");
    }
}

?>

<?php

$res = "";

if(count($tplData['data']['articles']) > 0 && count($tplData['data']['reviews']) > 0){
    foreach($tplData['data']['articles'] as $a){
        $res .= "<div class='container shadow rounded border border-light p-2 mt-4 mb-2 w-75'>";
        if($a['schvalen'] == 2){
            $res .= "<div class='alert-danger rounded p-1 text-center'>Článek zamítnut</div>";
        } else if ($a['schvalen'] == 1){
            $res .= "<div class='alert-success rounded p-1 text-center'>Článek schválen</div>";
        } else if ($a['schvalen'] == 0){
            $res .= "<div class='alert-warning rounded p-1 text-center'>Článek čeká na hodnocení</div>";
        }
        $filename = "uploads\\".$a['pdf'];
        $res .= "
        <h5>$a[autor]: $a[nazev]</h5>
        <p><strong>Abstrakt: </strong>$a[abstrakt]</p>";

        $review = null;
        foreach ($tplData['data']['reviews'] as $r){
            if($r['clanek_id'] == $a['clanek_id']){
                $review = $r;
                break;
            }
        }

        if($a['schvalen'] == 1 || $a['schvalen'] == 2){
            $res .= "
            <div class='row'>
                <div class='col-3'>
                    <label class='form-label' for='kvalita_obsahu'>Kvalita obsahu: </label>
                    <input name='kvalita_obsahu'  id='kvalita_obsahu' type='number' min='0' max='5' value='$review[kvalita_obsahu]' disabled>
                </div>
                <div class='col-3'>
                    <label class='form-label' for='uroven'>Úroveň: </label>
                    <input name='uroven'  id='uroven' type='number' min='0' max='5' value='$review[uroven]' disabled>
                </div>
                <div class='col-3'>
                    <label class='form-label' for='novost'>Novost: </label>
                    <input name='novost'  id='novost' type='number' min='0' max='5' value='$review[novost]' disabled>  
                </div>
                <div class='col-3'>
                    <label class='form-label' for='kvalita_jazyka'>Kvalita jazyka: </label>
                    <input name='kvalita_jazyka'  id='kvalita_jazyka' type='number' min='0' max='5' value='$review[kvalita_jazyka]' disabled>  
                </div>
            </div>

            <label class='form-label' for='komentar'>Komentář:</label>
            <textarea name='komentar' class='form-control form-control-sm me-2 font-monospace' id='address' rows='4'
                placeholder='Místo pro Váš komentář...' disabled>$review[komentar]</textarea>

    ";
        } else {
            $res .= "
        <form action='' method='post'>
            <div class='row'>
                <div class='col-3'>
                    <label class='form-label' for='kvalita_obsahu'>Kvalita obsahu: </label>
                    <input name='kvalita_obsahu'  id='kvalita_obsahu' type='number' min='0' max='5' value='$review[kvalita_obsahu]'>
                </div>
                <div class='col-3'>
                    <label class='form-label' for='uroven'>Úroveň: </label>
                    <input name='uroven'  id='uroven' type='number' min='0' max='5' value='$review[uroven]'>
                </div>
                <div class='col-3'>
                    <label class='form-label' for='novost'>Novost: </label>
                    <input name='novost'  id='novost' type='number' min='0' max='5' value='$review[novost]'>  
                </div>
                <div class='col-3'>
                    <label class='form-label' for='kvalita_jazyka'>Kvalita jazyka: </label>
                    <input name='kvalita_jazyka'  id='kvalita_jazyka' type='number' min='0' max='5' value='$review[kvalita_jazyka]'> 
                </div>                
            </div>

            <label class='form-label' for='komentar'>Komentář:</label>
            <textarea name='komentar' class='form-control form-control-sm me-2 font-monospace' id='address' rows='4'
                placeholder='Místo pro Váš komentář...'>$review[komentar]</textarea>
                   
            <input class='mt-2' type='hidden' name='recenze_id' value='$review[recenze_id]'>
            <button class='btn btn-success mt-2' type='submit' name='odeslat' value='odeslat'>Odeslat recenzi</button>
        </form>
    ";
        }

        $res .= "<div class='btn btn-primary mt-2' id='$a[clanek_id]_btn' onclick='showIFrame($a[clanek_id])'>Zobrazit PDF</div>
        <iframe style='display: none' src='$filename' id='$a[clanek_id]' width='100%' height='500px'></iframe>
    </div>";
    }
} else {
    $res .= "
        <div class='container rounded p-5 m-auto bg-primary bg-opacity-50 shadow text-center'>
            <h3 class='m-auto'>Nemáte přiděleny žádné recenze.</h3>
        </div>
    ";
}

$res .= '    <script type="text/javascript" src="js/show-pdf.js"></script>
             <script type="text/javascript" src="utils/ckeditor/ckeditor.js"></script>';

echo $res;

?>