/**
 * Kontroluje shodnost zadanych hesel a upravuje ohraniceni vstupnich poli.
 */
function checkPassword(){
    const firstField = document.getElementById("password-first");
    const secondField = document.getElementById("password-second");

    if(secondField.value != ""){
        if(firstField.value != secondField.value){
            firstField.style.borderColor = "red";
            secondField.style.borderColor = "red";
        } else {
            firstField.style.borderColor = "green";
            secondField.style.borderColor = "green";
        }
    }
}