/**
 * Kontroluje, zda je vyplnen registracni formular a podle toho zakaze/povoli registracni tlacitko.
 */
function checkFill(){
    const login = document.getElementById("login");
    const email = document.getElementById("email");
    const password1 = document.getElementById("password-first");
    const password2 = document.getElementById("password-second");
    const name = document.getElementById("name");
    const surname = document.getElementById("surname");
    const address = document.getElementById("address");

    const submitButton = document.getElementById("register");

    if(login.value == "" || email.value == "" || password1.value == "" || password2.value == "" || name.value == ""
        || surname.value == "" || address.value == ""){
        submitButton.disabled = true;
    } else {
        submitButton.disabled = false;
    }
}