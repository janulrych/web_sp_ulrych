/**
 * Kontroluje vyplneni profilu uzivatele a na zaklade toho zakaze/povoli tlacitko upravit.
 */
function checkFill(){
    const email = document.getElementById("email");
    const name = document.getElementById("name");
    const surname = document.getElementById("surname");
    const address = document.getElementById("address");

    const submitButton = document.getElementById("upravit");

    if(email.value == "" || name.value == "" || surname.value == "" || address.value == ""){
        submitButton.disabled = true;
    } else {
        submitButton.disabled = false;
    }
}