/**
 * Zobrazi/skryje okno s pdf souborem.
 * @param id    ID okna.
 */
function showIFrame(id) {
    let iframe = document.getElementById(id);
    let button = document.getElementById(id + "_btn");
    console.log(iframe.style.display);
    if(iframe.style.display=="none"){
        iframe.style.display="block";
        button.innerText = "Skrýt PDF"
    } else {
        iframe.style.display="none";
        button.innerText = "Zobrazit PDF"
    }
}