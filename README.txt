Semestrální práce z předmětu KIV/WEB

Adresářová struktura aplikace:

- root
    - app                                   (MVC struktura, třída AppStart)
        - Controllers                       (kontrolery)
        - Models                            (modely pro práci s DB a session)
        - Views                             (šablony)
    - assets                                (obrázky, loga)
    - db-definition                         (skript pro import databáze)
    - js                                    (JavaScript soubory)
        - form-check                        (JavaScripty pro kontrolu formulářů)
    - uploads                               (PDF soubory uživatelů)
    - utils                                 (FontAwesome a Bootstrap)